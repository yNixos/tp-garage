import {Vehicule} from "./Vehicule";
import {Moteur} from "../Interfaces/Moteur";

export class Camion extends Vehicule {


    constructor(marque: string, moteur: Moteur) {
        super(marque, moteur);
    }

    /**
     * retourne j'attache une remorque au camion
     */
    public attacherRemorque(): void {
        console.log('J\'attache la remorque');
    }

    /**
     * retourne je detache une remorque au camion
     */
    public detacherRemorque(): void {
        console.log('Je detache la remorque');
    }

    /**
     * affiche la marque du camion
     */
    afficherVehicule() {
        console.log(` Le camion de marque : ${this.marque}`);
    }

}