import {Vehicule} from "./Vehicule";
import {Garagiste} from "./Garagiste";
import {Moto} from "./Moto";
import {Voiture} from "./Voiture";
import {Camion} from "./Camion";

export class Garage {

    public _Vehicule: Vehicule[] = [];
    public _Garagiste: Garagiste[] = [];


    constructor(...Garagiste: Garagiste[]) {
        this._Garagiste = Garagiste;
    }

    /**
     * Ajoute un vehicule a la liste des vehicules du garage
     * @param vehicule
     */
    public ajouterVehicule(vehicule: Vehicule): void {
        this._Vehicule.push(vehicule);
    }
    /**
     * Retire un vehicule a la liste des vehicules du garage
     * @param vehicule
     */
    public retirerVehicule(vehicule: Vehicule): void {
        let index = this._Vehicule.indexOf(vehicule);

        this._Vehicule.splice(index, 1);
    }
    /**
     * Affiche le garage au complet
     */
    public afficherGarage(): void{
        this._Garagiste.map(garagiste=> {
            console.log(garagiste.sePresenter());
        });
        this._Vehicule.map(vehicule=>{
            console.log(vehicule.afficherVehicule());
        });
        /*console.log(this.afficherMotos());
        console.log(this.afficherVoitures());
        console.log(this.afficherCamions());*/

    }
    /**
     * affiche les motos de la listes des vehicules
     */
    public afficherMotos(): void{
        this._Vehicule.map(vehicule=> {
           if (vehicule instanceof Moto){
               console.log(vehicule.afficherVehicule());
           }
        });
    }
    /**
     * affiche les voitures de la listes des vehicules
     */
    public afficherVoitures(): void{
       this._Vehicule.map(vehicule=> {
             if (vehicule instanceof Voiture){
                 console.log(vehicule.afficherVehicule());
            }
        });
    }
    /**
     * affiche les camions de la listes des vehicules
     */
    public afficherCamions(): void {
        this._Vehicule.map(vehicule=> {
            if (vehicule instanceof Camion){
                console.log(vehicule.afficherVehicule());
            }
        });
    }

}