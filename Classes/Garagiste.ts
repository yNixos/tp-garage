import {Personne} from "./Personne";
import {Garage} from "./Garage";

export class Garagiste extends Personne{


    constructor(nom: string, age: number) {
        super(age, nom);
    }

    /**
     * retourne Je sais conduire
     */
    public conduire(): void{
        console.log("Je sais conduire");
    }

    /**
     * Affiche le garage en entier
     * @param garage
     */
    public presenterGarage(garage: Garage): void {
        garage.afficherGarage();
    }
}