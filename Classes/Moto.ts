import {Vehicule} from "./Vehicule";
import {Moteur} from "../Interfaces/Moteur";

export class Moto extends Vehicule {

    constructor(marque: string, moteur: Moteur) {
        super(marque, moteur);
    }

    /**
     * Fait un Wheel
     */
    public faireUnWheeling(): void{
        console.log('TMax en Y');
    }

    /**
     * affiche la marque de la moto
     */
    afficherVehicule() {
        console.log(` La moto de marque : ${this.marque}`);
    }

}