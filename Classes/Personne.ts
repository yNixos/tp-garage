export abstract class Personne {

    public _age: number;
    public _nom: string;


    constructor(age: number, nom: string) {
        this._age = age;
        this._nom = nom;
    }

    /**
     * retourne le nom et l'age de la personne
     */
    public sePresenter(): void{
        console.log(`nom : ${this._nom} \n age : ${this._age}`);
    }

}