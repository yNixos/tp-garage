import {Moteur} from "../Interfaces/Moteur";

export abstract class Vehicule {

    protected _marque: string;
    public _moteur: Moteur;

    constructor(marque: string, moteur: Moteur) {
        this._marque = marque;
        this._moteur = moteur;
    }

    /**
     * retourn que le vehicule avance
     */
    public avancer(){
        console.log('Le vehicule avance');
    }

    /**
     * retourn que le vehicule recule
     */
    public reculer(){
        console.log('Le vehicule recule');
    }
    /**
     * retourn que le vehicule freine
     */
    public freiner(){
        console.log('Le vehicule freine');
    }
    /**
     * affiche le vehicule
     */
    public afficherVehicule(){
        console.log(` Le vehicule de marque : ${this.marque}`);
    }

    get marque(): string {
        return this._marque;
    }

    set marque(value: string) {
        this._marque = value;
    }

    get moteur(): Moteur {
        return this._moteur;
    }

    set moteur(value: Moteur) {
        this._moteur = value;
    }

}