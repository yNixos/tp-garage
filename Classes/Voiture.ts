import {Vehicule} from "./Vehicule";
import {Moteur} from "../Interfaces/Moteur";

export class Voiture extends Vehicule{


    constructor(marque: string, moteur: Moteur) {
        super(marque, moteur);
    }

    /**
     * retourne la marque de la voiture
     */
    afficherVehicule() {
        console.log(` La voiture de marque : ${this.marque}`);
    }
}