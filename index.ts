import {Garagiste} from "./Classes/Garagiste";
import {Voiture} from "./Classes/Voiture";
import {MoteurEssence} from "./Classes/MoteurEssence";
import {Garage} from "./Classes/Garage";
import {Moto} from "./Classes/Moto";
import {MoteurElectrique} from "./Classes/MoteurElectrique";

const Benjamin : Garagiste = new Garagiste('Benjamin', 12);
const Benjamin2 : Garagiste = new Garagiste('Toto', 16);

const tutur : Voiture = new Voiture('Arthur', new MoteurEssence());
const momo : Moto = new Moto('Moomo', new MoteurElectrique());

/*Benjamin.sePresenter();
tutur.afficherVehicule();*/

let monGarage : Garage = new Garage(Benjamin, Benjamin2);
monGarage.ajouterVehicule(tutur);
monGarage.ajouterVehicule(momo);

//monGarage.retirerVehicule(momo);

monGarage.afficherGarage();